package com.example.demo.controller;


import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Controller
public class WelcomeController {

    @RequestMapping("/")
   
    public String home() {
    	
        return "index"; //view
    }

}
//ModelMap model,  HttpServletRequest request
